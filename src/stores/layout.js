import { defineStore } from "pinia";
import isScreen from "@/core/screenHelper";

export const layoutStore = defineStore("main", {
    state: () => {
        return {
            sidebarClose: false,
            sidebarStatic: false,
            sidebarActiveElement: null,
            chatOpen: false,
            chatNotificationIcon: false,
            chatNotificationPopover: false,
        };
    },
    actions: {
        toggleSidebar() {
            const nextState = !this.sidebarStatic;

            localStorage.sidebarStatic = nextState;
            this.sidebarStatic = nextState;

            if (!nextState && (isScreen("lg") || isScreen("xl"))) {
                this.sidebarClose = true;
            }
        },
        switchSidebar(value) {
            if (value) {
                this.sidebarClose = value;
            } else {
                this.sidebarClose = !this.sidebarClose;
            }
        },
        handleSwipe(e) {
            if ("ontouchstart" in window) {
                if (e.direction === 4) {
                    this.sidebarClose = false;
                }

                if (e.direction === 2 && !this.sidebarClose) {
                    this.sidebarClose = true;
                }
            }
        },
        changeSidebarActive(index) {
            this.sidebarActiveElement = index;
        },
    },
});
